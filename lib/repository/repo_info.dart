import 'package:dio/dio.dart';
import 'package:my_worker_app/config/config_api.dart';
import 'package:my_worker_app/function/token_lib.dart';
import 'package:my_worker_app/model/main/my_work_place_item_result.dart';

class RepoInfo{



  Future<MyWorKPlaceItemResult> getList() async {
    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';
    String _baseUrl = '$apiUri/v1/business-member/all/member-list';

    final response = await dio.get(
        _baseUrl,
        options: Options(
          // 데이터 없을 경우 다른 곳으로 안내 => false
            followRedirects: false,
            validateStatus: (status) {
              // 성공했을 경우 처리
              return status == 200;
            }
        )
    );

    return MyWorKPlaceItemResult.fromJson(response.data);
  }


}