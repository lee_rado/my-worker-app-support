class HomeQuickMenuItem {
  String businessId;

  HomeQuickMenuItem(this.businessId);

  factory HomeQuickMenuItem.fromJson(Map<String, dynamic> json) {
    return HomeQuickMenuItem(json['businessId']);
  }
}
