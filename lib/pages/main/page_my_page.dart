import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/component_appbar_base.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/pages/main/ask/page_ask_list.dart';

import 'store_main/my_page_info/page_my_info_edit.dart';
import 'store_main/my_page_info/page_my_info_schedule.dart';

class PageMyPage extends StatelessWidget {
  const PageMyPage({super.key});

  @override
  Widget build(BuildContext context) {
    bool _isMove = false;
    return Scaffold(
      appBar: ComponentAppbarBase(title: '마이페이지'),
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    width: 150,
                    margin:
                    EdgeInsets.only(left: 30, right: 0, top: 0, bottom: 0),
                    child: Image.asset('assets/img/logo_profile.png'),
                  ),
                  Container(
                    margin:
                    EdgeInsets.only(left: 40, right: 0, top: 50, bottom: 0),
                    height: 180,
                    width: MediaQuery.of(context).size.width - 230,
                    child: Column(
                      children: [
                        Column(
                          children: [
                            Row(
                              children: [
                                Container(
                                  child: Text(
                                    '나알바',
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w900),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                      left: 10, right: 0, top: 0, bottom: 0),
                                  child: Text(
                                    '[일반회원]',
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                        Column(
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width,
                              child: Text(
                                '아이디 : qwer3',
                                style: TextStyle(
                                  fontSize: 16,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              child: Text(
                                '생년월일 : 00.02.05',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 16,
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              child: Text(
                                '전화번호 : 010-0000-0000',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Container(
                              child: Column(
                                children: [
                                  ElevatedButton(
                                    onPressed: () {
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  PageMyInfoEdit()));
                                    },
                                    style: ElevatedButton.styleFrom(
                                        primary:
                                        colorNormal,
                                        onPrimary:
                                        colorDark),
                                    child: const Text(
                                      '수정하기',
                                      style: TextStyle(
                                        fontSize: 20,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        shadows: [
                                          Shadow(
                                            blurRadius: 5.0,
                                            color: Colors.black38,
                                            offset: Offset(2.0, 2.0),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
              Column(
                children: [
                  Container(
                      width: MediaQuery.of(context).size.width,
                      height: 150,
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 2, //선의 너비
                          color: Color.fromRGBO(64, 114, 175, 1),
                        ),
                      ),
                      child: Image.asset('assets/img/event_banner.png',
                          width: MediaQuery.of(context).size.width,
                          fit: BoxFit.cover)),
                ],
              ),
              //마이페이지 메뉴시작
              Column(
                children: [
                  Container(
                      padding: EdgeInsets.all(25),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        border: Border(
                          bottom:
                          BorderSide(color: Colors.grey.withOpacity(0.3)),
                        ),
                      ),
                      child: Container(
                        child: TextButton(
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => PageMyInfoSchedule()));
                          },
                          child: Text(
                            '근무일정',
                            style: TextStyle(
                              fontSize: 16,
                              color: Color.fromRGBO(100, 100, 100, 1),
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )),
                  // Container(
                  //     padding: EdgeInsets.all(25),
                  //     width: MediaQuery.of(context).size.width,
                  //     decoration: BoxDecoration(
                  //       border: Border(
                  //         bottom:
                  //         BorderSide(color: Colors.grey.withOpacity(0.3)),
                  //       ),
                  //     ),
                  //     child: Container(
                  //       child: TextButton(
                  //         onPressed: () {
                  //           Navigator.of(context).push(MaterialPageRoute(
                  //               builder: (context) => PageMyInfoWorkPlace()));
                  //         },
                  //         child: Text(
                  //           '근무지 등록',
                  //           style: TextStyle(
                  //             fontSize: 16,
                  //             color: Color.fromRGBO(100, 100, 100, 1),
                  //             fontWeight: FontWeight.bold,
                  //           ),
                  //           textAlign: TextAlign.center,
                  //         ),
                  //       ),
                  //     )
                  // ),
                  Container(
                      padding: EdgeInsets.all(25),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        border: Border(
                          bottom:
                          BorderSide(color: Colors.grey.withOpacity(0.3)),
                        ),
                      ),
                      child: Container(
                        child: TextButton(
                          onPressed: () {},
                          child: Text(
                            '공지사항',
                            style: TextStyle(
                              fontSize: 16,
                              color: Color.fromRGBO(100, 100, 100, 1),
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )),
                  Container(
                      padding: EdgeInsets.all(25),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        border: Border(
                          bottom:
                          BorderSide(color: Colors.grey.withOpacity(0.3)),
                        ),
                      ),
                      child: Container(
                        child: TextButton(
                          onPressed: () { Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PageAskList()));},
                          child: Text(
                            '고객센터',
                            style: TextStyle(
                              fontSize: 16,
                              color: Color.fromRGBO(100, 100, 100, 1),
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
