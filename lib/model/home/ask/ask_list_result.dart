import 'package:my_worker_app/model/home/ask/ask_list.dart';

class AskListResult {
  num totalCount;
  num totalPage;
  num currentPage;
  List<AskList>? list;

  AskListResult(this.totalCount, this.totalPage, this.currentPage, this.list);

  factory AskListResult.fromJson(Map<String, dynamic> json) {
    return AskListResult(
      json['totalCount'],
      json['totalPage'],
      json['currentPage'],
      json['list'] != null ? (json['list'] as List).map((e) => AskList.fromJson(e)).toList() : [],
    );
  }
}