import 'package:dio/dio.dart';
import 'package:my_worker_app/config/config_api.dart';
import 'package:my_worker_app/model/login/login_request.dart';
import 'package:my_worker_app/model/login/login_result.dart';

class RepoLogin {

  // 로그인 하기
  Future<LoginResult> doLogin(LoginRequest request) async {
    Dio dio = Dio();
    String _baseUrl = '$apiUri/v1/login/app/user';
    final response = await dio.post(
        _baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    print(response);

    return LoginResult.fromJson(response.data);
  }

}
