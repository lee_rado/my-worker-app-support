class AskDetail {
  num id;
  String memberAskTitle;
  // 문의 작성 날짜
  // 문의 내용
  // 문의 이미지
  String answerTitle;
  String dateAnswer;
  String answerImgUrl;
  String answerContent;

  AskDetail (
      this.id,
      this.memberAskTitle,
      // 문의 작성 날짜
      // 문의 내용
      // 문의 이미지
      this.answerTitle,
      this.dateAnswer,
      this.answerImgUrl,
      this.answerContent,
      );

  factory AskDetail.fromJson(Map<String, dynamic> json) {
    return AskDetail(
      json['id'],
      json['memberAskTitle'],
      // 문의 작성 날짜
      // 문의 내용
      // 문의 이미지
      json['answerTitle'],
      json['dateAnswer'],
      json['answerImgUrl'],
      json['answerContent'],
    );
  }
}