

import 'package:my_worker_app/model/member/member_response.dart';

class MemberDetailResult {
  String msg;
  num code;
  MemberResponse data;

  MemberDetailResult(this.msg, this.code, this.data);

  factory MemberDetailResult.fromJson(Map<String, dynamic> json) {
    return MemberDetailResult(
      json['msg'],
      json['code'],
      MemberResponse.fromJson(json['data'])
    );
  }
}