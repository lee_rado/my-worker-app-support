import 'member_search_name_item.dart';

class MemberSearchNameResult {
  String msg;
  num code;
  List<MemberSearchNameItem> list;
  num totalCount;

  MemberSearchNameResult(this.msg, this.code, this.list, this.totalCount);

  factory MemberSearchNameResult.fromJson(Map<String, dynamic> json) {
    return MemberSearchNameResult(
      json['msg'],
      json['code'],
      json['list'] != null ?
      (json['list'] as List).map((e) => MemberSearchNameItem.fromJson(e)).toList()
          : [],
      json['totalCount'],
    );
  }
}
