import 'package:flutter/material.dart';
import '../../model/home/my_info.dart';
import 'package:my_worker_app/config/color.dart';

class ComponentsMyPage extends StatelessWidget {
  const ComponentsMyPage({
    super.key,
    required this.callback,
    required this.myInfo,
  });

  final VoidCallback callback;
  final MyInfo myInfo;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  child: Image.asset(
                    'assets/img/people.png',
                  ),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(myInfo.name),
                      Text(myInfo.memberType),
                      Text(myInfo.username),
                      Text(myInfo.dateBirth),
                      Text(myInfo.phoneNumber),
                    ],
                  ),
                  width: 280,
                )
              ],
            ),
            Divider(
              color: colorLight,
            ),
          ],
        ),
      ),
    );
  }
}
