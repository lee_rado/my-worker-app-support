import 'package:flutter/material.dart';
import 'package:my_worker_app/config/color.dart';

class ComponentQuickItem extends StatelessWidget {
  const ComponentQuickItem(
      {super.key, required this.callback, required this.title, });

  final VoidCallback callback;
  final String title;


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Text(
          title,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w500, fontSize: 14),
        ),
        alignment: Alignment.center,
        width: 100,
        height: 100,
        padding: EdgeInsets.all(5),
        margin: EdgeInsets.all(3),
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 5,
                blurRadius: 10,
                offset: Offset(0, 2), // changes position of shadow
              ),
            ],
            border: Border.all(width: 5.0, color: colorNormal),
            color: colorNormal,
            borderRadius: BorderRadius.circular(50)),
        // margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
      ),
    );
  }
}
