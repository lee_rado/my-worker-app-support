import 'package:flutter/material.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/home/quickmenu_item.dart';



class ComponentsQuickMenu extends StatelessWidget {
  const ComponentsQuickMenu({
    super.key,
    required this.callback,
    required this.quickMenu,
  });

  final VoidCallback callback;
  final QuickMenu quickMenu;


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Container(
              child: Image.asset(quickMenu.imgUrl, width: 40, height: 40, color: colorNormal,),
              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
            ),

            Text(quickMenu.menu, style: TextStyle(fontSize: 13, color: colorNormal,fontWeight: FontWeight.bold))
          ],
        ),
        width: 100,
        height: 100,
        decoration: BoxDecoration(
            border: Border.all(color: colorNormal, width: 3),
            borderRadius: BorderRadius.circular(50)),
        margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
      ),

    );
  }
}

