class CommonResult {
  String msg;
  num code;
  bool isSuccess;

  CommonResult(this.msg, this.code, this.isSuccess);

  factory CommonResult.fromJson(Map<String, dynamic> json) {
    return CommonResult(json['msg'], json['code'], json['isSuccess']);
  }
}
