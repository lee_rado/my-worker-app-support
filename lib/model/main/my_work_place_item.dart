class MyWorkPlaceItem {
  num businessId;
  String businessName;

  MyWorkPlaceItem(this.businessId,this.businessName);

  factory MyWorkPlaceItem.fromJson(Map<String, dynamic> json) {
    return MyWorkPlaceItem(
        json['businessId'],
        json['businessName']);
  }
}
