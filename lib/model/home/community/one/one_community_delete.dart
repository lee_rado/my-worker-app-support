class OneCommunityDelete {
  num id;

  OneCommunityDelete(
      this.id
      );

  factory OneCommunityDelete.fromJson(Map<String, dynamic> json) {
    return OneCommunityDelete(
      json['id']
    );
  }
}