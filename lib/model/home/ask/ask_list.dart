class AskList {
  num id;
  String title;
  String dateMemberAsk;
  String memberName;
  // String questionImgUrl;
  String isAnswer;

  AskList(
      this.id,
      this.title,
      this.dateMemberAsk,
      this.memberName,
      // this.questionImgUrl,
      this.isAnswer
  );

  factory AskList.fromJson(Map<String, dynamic> json) {
    return AskList(
      json['id'],
      json['title'],
      json['dateMemberAsk'],
      json['memberName'],
      // json['questionImgUrl'],
      json['isAnswer'],
    );
  }
}