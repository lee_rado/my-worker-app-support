import 'package:my_worker_app/model/main/my_work_place_item.dart';

class MyWorKPlaceItemResult {
  String msg;
  num code;
  List<MyWorkPlaceItem>? list;

  MyWorKPlaceItemResult(
      this.msg,
      this.code,
      this.list
      );

  factory MyWorKPlaceItemResult.fromJson(Map<String, dynamic> json) {
    return MyWorKPlaceItemResult(
        json['msg'],
        json['code'],
        json['list'] != null ? (json['list'] as List).map((e) => MyWorkPlaceItem.fromJson(e)).toList() : [],
    );
  }
}