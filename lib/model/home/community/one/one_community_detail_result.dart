import 'package:my_worker_app/model/home/community/one/one_community_detail.dart';

class OneCommunityDetailResult {
  String msg;
  num code;
  OneCommunityDetail data;

  OneCommunityDetailResult(
      this.msg,
      this.code,
      this.data
      );

  factory OneCommunityDetailResult.fromJson(Map<String, dynamic> json) {
    return OneCommunityDetailResult(
        json['msg'],
        json['code'],
        OneCommunityDetail.fromJson(json['data'])
    );
  }
}