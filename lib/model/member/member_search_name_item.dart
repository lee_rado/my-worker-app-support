class MemberSearchNameItem {
  String username; //아이디
  String nickName; //닉네임
  String name; //이름
  String gender; //성별

  MemberSearchNameItem(
      this.username,
      this.nickName,
      this.name,
      this.gender,
      );

  factory MemberSearchNameItem.fromJson(Map<String, dynamic> json) {
    return MemberSearchNameItem (
      json['username'],
      json['nickName'],
      json['name'],
      json['gender'],
    );
  }


}