class AskCreateRequest {
  String title;
  String content;
  String? questionImgUrl;

  AskCreateRequest(this.title, this.content, this.questionImgUrl);

  // 등록이므로 toJson으로 하기
  Map<String, dynamic> toJson() {

    // data에 값 넣어주기
    Map<String, dynamic> data = Map<String, dynamic>();

    // 어차피 해당 지역에 같이 있어서 this 생략 가능
    data['title'] = title;
    data['content'] = content;
    data['questionImgUrl'] = questionImgUrl;

    return data;
  }
}