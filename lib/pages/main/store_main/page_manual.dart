import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/component_appbar_base.dart';
class PageManual extends StatefulWidget {
  const PageManual({super.key});

  @override
  State<PageManual> createState() => _PageManualState();
}

class _PageManualState extends State<PageManual> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarBase(title: '메뉴얼'),
      body: SingleChildScrollView(child:
        Text('메뉴얼페이지'),),
    );
  }
}
