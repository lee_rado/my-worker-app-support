class OneCommunityComment {
  String content;
  String dateContent;

  OneCommunityComment(
      this.content,
      this.dateContent
      );

  factory OneCommunityComment.fromJson(Map<String, dynamic> json) {
    return OneCommunityComment(
      json['content'],
      json['dateContent'],
    );
  }
}