import 'package:my_worker_app/model/home/ask/ask_detail.dart';

class AskDetailResult {
  String msg;
  num code;
  AskDetail data;

  AskDetailResult(
      this.msg,
      this.code,
      this.data
      );

  factory AskDetailResult.fromJson(Map<String, dynamic> json) {
    return AskDetailResult(
        json['msg'],
        json['code'],
        AskDetail.fromJson(json['data'])
    );
  }
}