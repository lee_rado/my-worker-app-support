class MyInfo {

  // 이름, 아이디. 생년월일, 전화번호, 회원가입유형
  String name;
  String username;
  String dateBirth;
  String phoneNumber;
  String memberType;

  MyInfo(
      this.name,
      this.username,
      this.dateBirth,
      this.phoneNumber,
      this.memberType,
      );

  factory MyInfo.fromJson(Map<String, dynamic> json) {
    return MyInfo(
      json['name'],
      json['username'],
      json['dateBirth'],
      json['phoneNumber'],
      json['memberType'],
    );
  }
}