import 'package:flutter/material.dart';
class ComponentCommunityContentsTogether extends StatefulWidget {
  const ComponentCommunityContentsTogether({super.key});

  @override
  State<ComponentCommunityContentsTogether> createState() => _ComponentCommunityContentsTogetherState();
}

class _ComponentCommunityContentsTogetherState extends State<ComponentCommunityContentsTogether> {
  @override
  Widget build(BuildContext context) {
    return  Container(
      height: MediaQuery.of(context).size.height,
      // ListView.builder을 선언하기 전에 먼저 높이를 설정해줘서 오류를 피해줍니다.
      child:ListView.builder(
        shrinkWrap: true,

        // 현재 표시되는 아이템에 맞게 크기를 자동 조절
        padding: EdgeInsets.all(5),
        //몇번을 수행할것인가에 대해서 미리 생성해둔 imageList의 길이를 가져오는 부분
        itemCount: 1000,
        itemBuilder: (BuildContext context, int index) {

          return Column(
            // index로 imageList 안에 있는 이미지들을 모두 가져옵니다
            //Image.asset 다른 옵션들로 크기 등 조정할 수 있습니다
            children: [Text('안녕하세요 갑과 을 게시글입니다.')],
          );
        },
      ),
    );
  }
}
