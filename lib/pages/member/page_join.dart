

import 'package:flutter/material.dart' '';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/pages/member/page_login.dart';

class PageJoin extends StatefulWidget {
  const PageJoin({super.key});

  @override
  State<PageJoin> createState() => _PageJoinState();
}

class _PageJoinState extends State<PageJoin> {
  final isSelected = <bool>[false, false];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorNormal,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 1,
                    height: MediaQuery.of(context).size.height / 11,
                    alignment: Alignment.centerRight,
                    child: Image.asset(
                      'assets/img/logo_t.png',
                      width: 80,
                    ),
                  )
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      BorderRadius.only(topRight: Radius.circular(80)),
                ),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 10, bottom: 20),
                      child: Text(
                        '회원가입',
                        style: TextStyle(
                            fontSize: 30,
                            color: colorNormal,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    /** 제목 **/
                    /** form 시작 **/
                    Column(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 1.3,
                          height: 90,
                          padding: EdgeInsets.all(10),
                          child: TextField(
                            decoration: InputDecoration(
                              fillColor: Colors.grey,
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50),
                                  //모서리 둥글게
                                  borderSide: BorderSide(
                                    color: colorNormal,
                                    width: 2.0,
                                  )),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50),
                                  //모서리 둥글게
                                  borderSide: BorderSide(
                                    color: colorNormal,
                                    width: 2.0,
                                  )),
                              labelText: ' 아이디',
                              labelStyle: TextStyle(
                                color: Color.fromRGBO(190, 190, 190, 1),
                              ),
                            ),
                          ),
                        ),
                        /** 아이디 입력 끝 **/

                        Container(
                          width: MediaQuery.of(context).size.width / 1.3,
                          height: 90,
                          padding: EdgeInsets.all(10),
                          child: TextField(
                            decoration: InputDecoration(
                              fillColor: Colors.grey,
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50),
                                  //모서리 둥글게
                                  borderSide: BorderSide(
                                    color: colorNormal,
                                    width: 2.0,
                                  )),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50),
                                  //모서리 둥글게
                                  borderSide: BorderSide(
                                    color: colorNormal,
                                    width: 2.0,
                                  )),
                              labelText: ' 비밀번호',
                              labelStyle: TextStyle(
                                color: Color.fromRGBO(190, 190, 190, 1),
                              ),
                            ),
                            obscureText: true,
                          ),
                        ),
                        /** 비밀번호 입력 끝 **/

                        Container(
                          width: MediaQuery.of(context).size.width / 1.3,
                          height: 90,
                          padding: EdgeInsets.all(10),
                          child: TextField(
                            decoration: InputDecoration(
                              fillColor: Colors.grey,
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50),
                                  //모서리 둥글게
                                  borderSide: BorderSide(
                                    color: colorNormal,
                                    width: 2.0,
                                  )),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50),
                                  //모서리 둥글게
                                  borderSide: BorderSide(
                                    color: colorNormal,
                                    width: 2.0,
                                  )),
                              labelText: ' 비밀번호 확인',
                              labelStyle: TextStyle(
                                color: Color.fromRGBO(190, 190, 190, 1),
                              ),
                            ),
                            obscureText: true,
                          ),
                        ),
                        /** 비밀번호 확인 입력 끝 **/

                        Container(
                          width: MediaQuery.of(context).size.width / 1.3,
                          height: 90,
                          padding: EdgeInsets.all(10),
                          child: TextField(
                            keyboardType: TextInputType.phone,
                            // 전화번호 입력에 적합한 키보드
                            decoration: InputDecoration(
                              fillColor: Colors.grey,
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50),
                                  //모서리 둥글게
                                  borderSide: BorderSide(
                                    color: colorNormal,
                                    width: 2.0,
                                  )),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50),
                                  //모서리 둥글게
                                  borderSide: BorderSide(
                                    color: colorNormal,
                                    width: 2.0,
                                  )),
                              labelText: ' 전화번호',
                              labelStyle: TextStyle(
                                color: Color.fromRGBO(190, 190, 190, 1),
                              ),
                            ),
                          ),
                        ),
                        /** 전화번호 입력 끝 **/
                        Row(
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width / 2,
                              height: 90,
                              padding: EdgeInsets.all(10),
                              margin: EdgeInsets.fromLTRB(50, 0, 0, 0),
                              child: TextField(
                                decoration: InputDecoration(
                                  fillColor: Colors.grey,
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(50),
                                      //모서리 둥글게
                                      borderSide: BorderSide(
                                        color: colorNormal,
                                        width: 2.0,
                                      )),
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(50),
                                      //모서리 둥글게
                                      borderSide: BorderSide(
                                        color: colorNormal,
                                        width: 2.0,
                                      )),
                                  labelText: ' 이름',
                                  labelStyle: TextStyle(
                                    color: Color.fromRGBO(190, 190, 190, 1),
                                  ),
                                ),
                              ),
                            ),
                            /** 이름 입력 끝 **/
                            /** 남녀 토글 버튼 **/
                            ToggleButtons(
                              color: Colors.black.withOpacity(0.60),
                              selectedColor: Color(0xFF6200EE),
                              selectedBorderColor: Color(0xFF6200EE),
                              fillColor: Color(0xFF6200EE).withOpacity(0.08),
                              splashColor: Color(0xFF6200EE).withOpacity(0.12),
                              hoverColor: Color(0xFF6200EE).withOpacity(0.04),
                              borderRadius: BorderRadius.circular(4.0),
                              constraints: BoxConstraints(minHeight: 36.0),
                              isSelected: isSelected,
                              onPressed: (index) {
                                // Respond to button selection
                                setState(() {
                                  isSelected[index] = !isSelected[index];
                                });
                              },
                              children: [
                                Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 10.0),
                                  child: Text('남자'),
                                ),
                                Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 10.0),
                                  child: Text('여자'),
                                )
                              ],
                            ),

                            /** 남녀 토글 버튼 끝**/
                          ],
                        ),
                        Container(
                            width: MediaQuery.of(context).size.width / 1.3,
                            height: 90,
                            padding: EdgeInsets.all(10),
                            child: Column(
                              children: [
                                TextField(
                                  decoration: InputDecoration(
                                    fillColor: Colors.grey,
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(50),
                                        //모서리 둥글게
                                        borderSide: BorderSide(
                                          color: colorNormal,
                                          width: 2.0,
                                        )),
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(50),
                                        //모서리 둥글게
                                        borderSide: BorderSide(
                                          color: colorNormal,
                                          width: 2.0,
                                        )),
                                    labelText: ' 생년월일 ex) 910101',
                                    labelStyle: TextStyle(
                                      color: Color.fromRGBO(190, 190, 190, 1),
                                    ),
                                  ),
                                ),
                              ],
                            )),
                        /** 생년월일 입력 끝 **/

                        Container(
                            width: MediaQuery.of(context).size.width / 1.3,
                            height: 90,
                            padding: EdgeInsets.all(10),
                            child: Column(
                              children: [
                                TextField(
                                  decoration: InputDecoration(
                                    fillColor: Colors.grey,
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(50),
                                        //모서리 둥글게
                                        borderSide: BorderSide(
                                          color: colorNormal,
                                          width: 2.0,
                                        )),
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(50),
                                        //모서리 둥글게
                                        borderSide: BorderSide(
                                          color: colorNormal,
                                          width: 2.0,
                                        )),
                                    labelText: ' 주소',
                                    labelStyle: TextStyle(
                                      color: Color.fromRGBO(190, 190, 190, 1),
                                    ),
                                  ),
                                ),
                              ],
                            )),
                        /** 주소 입력 끝 **/

                        /**가입하기 버튼**/
                        Container(
                            width: MediaQuery.of(context).size.width / 2,
                            height: MediaQuery.of(context).size.height / 20,
                            padding: EdgeInsets.all(5),
                            child: ElevatedButton(
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => PageLogin()));
                              },
                              style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.resolveWith(
                                        (Set<MaterialState> states) {
                                  if (states.contains(MaterialState.pressed)) {
                                    return Color.fromRGBO(63, 114, 175, 1);
                                  }
                                }),
                                foregroundColor:
                                    MaterialStateProperty.resolveWith(
                                        (Set<MaterialState> states) {
                                  if (states.contains(MaterialState.pressed)) {
                                    return Color.fromRGBO(255, 255, 255, 1);
                                  }
                                }),
                              ),
                              child: Text('가입하기'),
                            )),
                        /**로그인하러가기 버튼**/

                        Container(
                            width: MediaQuery.of(context).size.width / 2,
                            height: MediaQuery.of(context).size.height / 20,
                            padding: EdgeInsets.all(5),
                            child: ElevatedButton(
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => PageLogin()));
                              },
                              style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.resolveWith(
                                        (Set<MaterialState> states) {
                                  if (states.contains(MaterialState.pressed)) {
                                    return Color.fromRGBO(63, 114, 175, 1);
                                  }
                                }),
                                foregroundColor:
                                    MaterialStateProperty.resolveWith(
                                        (Set<MaterialState> states) {
                                  if (states.contains(MaterialState.pressed)) {
                                    return Color.fromRGBO(255, 255, 255, 1);
                                  }
                                }),
                              ),
                              child: Text('로그인하러가기'),
                            )),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
