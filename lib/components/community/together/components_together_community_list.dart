import 'package:flutter/material.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/home/community/one/one_community_list.dart';


class ComponentsTogetherCommunityList extends StatelessWidget {
  const ComponentsTogetherCommunityList({
    super.key,
    required this.callback,
    required this.togetherCommunityList,
  });

  final VoidCallback callback;
  final OneCommunityList togetherCommunityList;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${togetherCommunityList.title}',
                    style: TextStyle(
                        color: colorNormal,
                        fontSize: fontSizeMedium
                    ),
                  ),
                ],
              ),
              margin: EdgeInsets.fromLTRB(10, 0, 0, 5),
            ),
            Container(
              color: colorLight,
              height: 1,
            ),
            Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Text(
                      '${togetherCommunityList.boardImgUrl}',
                      style: TextStyle(
                          fontSize: fontSizeSmall
                      ),
                    ),
                  ),
                ],
              ),
              margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
            ),
            Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Text(
                      '${togetherCommunityList.content}',
                      style: TextStyle(
                          fontSize: fontSizeSmall
                      ),
                    ),
                  ),
                ],
              ),
              margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
            ),
            Container(
              child: Row(
                children: [
                  Text('댓글 '),
                  Text('18개', style: TextStyle(color: colorNormal, fontWeight: FontWeight.bold),),
                ],
              ),
              margin: EdgeInsets.fromLTRB(10, 20, 0, 0),
            ),
            Divider(
                color: colorNormal
            ),
          ],
        ),
        margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
        // color: Color.fromRGBO(245, 248, 255, 1.0),
        width: MediaQuery.of(context).size.width / 1.0,
      ),
    );
  }
}
