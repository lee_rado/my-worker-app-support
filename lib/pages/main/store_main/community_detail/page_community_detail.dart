import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/component_appbar_base.dart';

class PageCommunityDetail extends StatefulWidget {
  const PageCommunityDetail({super.key});

  @override
  State<PageCommunityDetail> createState() => _PageCommunityDetailState();
}

class _PageCommunityDetailState extends State<PageCommunityDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarBase(title: '커뮤니티'),
      body: SingleChildScrollView(
        child: Container(
          child: Text('게시글 상세보기'),
        ),
      ),
    );
  }
}
