 import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/component_appbar_base.dart';
class PageMyAttendance extends StatefulWidget {
  const PageMyAttendance({super.key});

  @override
  State<PageMyAttendance> createState() => _PageMyAttendanceState();
}

class _PageMyAttendanceState extends State<PageMyAttendance> {
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      appBar: ComponentAppbarBase(title: '출퇴근 기록'),
      body: SingleChildScrollView(
        child:
        Text('출퇴근 기록 페이지'),
      ),
    );
  }
}
